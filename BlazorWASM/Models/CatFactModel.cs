﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorWASM.Models
{
    public class CatFactModel
    {
        public string _id { get; set; }
        public int _v { get; set; }
        
        public string Text { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool Deleted { get; set; }
        public string Source { get; set; }
        public bool Used { get; set; }

    }
}
