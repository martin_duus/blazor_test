﻿using BlazorWASM.Models;
using BlazorWASM.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorWASM.ViewModels
{
    public interface ICatFactViewModel
    {
        Task GetCatFact();

        CatFactModel[] Cats { get; }
    }

    public class CatFactViewModel : ICatFactViewModel
    {
        private ICatDataService _catDataSerive;

        public CatFactViewModel(ICatDataService catData)
        {
            _catDataSerive = catData;
        }

        private BlazorWASM.Models.CatFactModel[] _cats;

        public BlazorWASM.Models.CatFactModel[] Cats
        {
            get { return _cats; }
            private set { _cats = value; }
        }


        public async Task GetCatFact()
        {
            _cats = await _catDataSerive.GetCatFact();
            foreach (var item in _cats)
            {
                Console.WriteLine(item.Text);
            }
        }
    }
}
