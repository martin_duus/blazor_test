﻿using BlazorWASM.Models;
using BlazorWASM.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorWASM.ViewModels
{
    public interface IFetchDataViewModel
    {
        WeatherForecast[] WeatherForecasts { get; set; }
        Task RetrieveForecastsAsync();
    }
    public class FetchDataViewModel : IFetchDataViewModel
    {
        private WeatherForecast[] _weatherForecasts;
        private IFetchDataService _fetchDataService;
        
        public FetchDataViewModel(IFetchDataService fetchDataService)
        {
            Console.WriteLine("FetchDataViewModel Constructor Executing");
            _fetchDataService = fetchDataService;
        }

        public WeatherForecast[] WeatherForecasts
        {
            get => _weatherForecasts;
            set => _weatherForecasts = value;
        }

        public async Task RetrieveForecastsAsync()
        {
            _weatherForecasts = await _fetchDataService.RetrieveForecastsAsync();
            Console.WriteLine("FetchDataViewModel Forecasts Retrieved");
        }
    }
}