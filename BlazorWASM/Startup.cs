using Microsoft.AspNetCore.Components.Builder;
using Microsoft.Extensions.DependencyInjection;
using BlazorWASM.ViewModels;
using BlazorWASM.Services;

namespace BlazorWASM
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IFetchDataViewModel, FetchDataViewModel>();
            services.AddTransient<IFetchDataService, FetchDataService>();

            services.AddTransient<ICatFactViewModel, CatFactViewModel>();
            services.AddTransient<ICatDataService, CatDataService>();


        }

        public void Configure(IComponentsApplicationBuilder app)
        {
            app.AddComponent<App>("app");
           
        }
    }
}
