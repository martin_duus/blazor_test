﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BlazorWASM.Models;
using Microsoft.AspNetCore.Components;

namespace BlazorWASM.Services
{
    public interface IFetchDataService
    {
        Task<WeatherForecast[]> RetrieveForecastsAsync();
    }
    public class FetchDataService : IFetchDataService
    {
        private HttpClient _http;

        public FetchDataService(HttpClient http)
        {
            _http = http;
        }

        public async Task<WeatherForecast[]> RetrieveForecastsAsync()
        {
            return await _http.GetJsonAsync<WeatherForecast[]>("sample-data/weather.json"); // api/SampleData/WeatherForecasts"); // sample-data/weather.json
        }
    }
}
