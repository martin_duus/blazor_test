﻿using BlazorWASM.Models;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace BlazorWASM.Services
{
    public interface ICatDataService
    {
        Task<CatFactModel[]> GetCatFact();
    }

    public class CatDataService : ICatDataService
    {
        private HttpClient _http;

        public CatDataService(HttpClient http)
        {
            _http = http;
        }

        public async Task<CatFactModel[]> GetCatFact()
        {
            return await _http.GetJsonAsync<CatFactModel[]>("sample-data/cat-fact.json"); //https://cat-fact.herokuapp.com/facts/random?amount=5");
        }
    }
}
